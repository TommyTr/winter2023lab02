class MethodsTest{
	
	public static void main(String[] args){
		
		int x = 5;
		//System.out.println(x);
		//methodNoInputNoReturn();
		//System.out.println(x);
		
		//System.out.println(x);
		//methodOneInputNoReturn(x+10);
		//System.out.println(x);
		
		//methodTwoInputNoReturn(2.5,1);
		
		//int b = methodNoInputReturnInt();
		//System.out.println(b);
		
		//double sqrt=sumSquareRoot(9,5);
		//System.out.println(sqrt);
		
		//String s1="java";
		//String s2="programming";
		//System.out.println(s1.length());
		//System.out.println(s2.length());
		
		System.out.println(SecondClass.addOne(50));
		SecondClass sc=new SecondClass();
		System.out.println(sc.addTwo(50));
	}
	
	public static void methodNoInputNoReturn(){
		
		System.out.println("I'm in a method that takes no input and returns nothing");
		int x = 20;
		System.out.println(x);
	}
	
	public static void methodOneInputNoReturn(int a){
		a-=5;
		System.out.println("Inside the method one input no return: "+a);
		
		
	}
	
	public static void methodTwoInputNoReturn(int a, double b){
		
		System.out.println(a);
		System.out.println(b);
	}
	
	public static int methodNoInputReturnInt(){
		return 5;
	}
	
	public static double sumSquareRoot(int num, int num2){
		int sum=num+num2;
		
		double result=Math.sqrt(sum);
		return result;
		
	}
}
	
		